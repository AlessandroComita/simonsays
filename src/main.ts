console.log("main ts");




// element grabbing

var greenButton = document.getElementById('green');
// console.log(greenButton);
var disc = document.getElementById('disc');
// console.log(disc);


// set variables


var gameList: string[] = [];
var game: string = '';




// functions definition

function lightsOut()
{
    disc?.setAttribute('src', './images/disc500-b.png');
}

function triggerGreen()
{
    disc?.setAttribute('src', './images/disc_light_green.png');
    setTimeout(lightsOut, 500);
    game = "green";
    gameList.push(game);
    console.log(gameList);
}

function triggerYellow()
{
    disc?.setAttribute('src', './images/disc_light_yellow.png');
    setTimeout(lightsOut, 500);
    game = "yellow";
    gameList.push(game);
    console.log(gameList);
}

function triggerBlue()
{
    disc?.setAttribute('src', './images/disc_light_blue.png');
    setTimeout(lightsOut, 500);
    game = "blue";
    gameList.push(game);
    console.log(gameList);
}

function triggerRed()
{
    disc?.setAttribute('src', './images/disc_light_red.png');
    setTimeout(lightsOut, 500);
    game = "red";
    gameList.push(game);
    console.log(gameList);
}