"use strict";
console.log("main ts");
// element grabbing
var greenButton = document.getElementById('green');
// console.log(greenButton);
var disc = document.getElementById('disc');
// console.log(disc);
// set variables
var gameList = [];
var game = '';
// functions definition
function lightsOut() {
    disc === null || disc === void 0 ? void 0 : disc.setAttribute('src', './images/disc500-b.png');
}
function triggerGreen() {
    disc === null || disc === void 0 ? void 0 : disc.setAttribute('src', './images/disc_light_green.png');
    setTimeout(lightsOut, 500);
    game = "green";
    gameList.push(game);
    console.log(gameList);
}
function triggerYellow() {
    disc === null || disc === void 0 ? void 0 : disc.setAttribute('src', './images/disc_light_yellow.png');
    setTimeout(lightsOut, 500);
    game = "yellow";
    gameList.push(game);
    console.log(gameList);
}
function triggerBlue() {
    disc === null || disc === void 0 ? void 0 : disc.setAttribute('src', './images/disc_light_blue.png');
    setTimeout(lightsOut, 500);
    game = "blue";
    gameList.push(game);
    console.log(gameList);
}
function triggerRed() {
    disc === null || disc === void 0 ? void 0 : disc.setAttribute('src', './images/disc_light_red.png');
    setTimeout(lightsOut, 500);
    game = "red";
    gameList.push(game);
    console.log(gameList);
}
